package com.wavelabs.ws.api;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.ws.model.Greeting;
import com.wavelabs.ws.service.GreetingService;

/**
 * Controller class for Greetings API.
 * 
 * @author bikshapathi
 *
 */
@RestController
public class GreetingController {
	@Autowired
	private GreetingService greetingService;

	/**
	 * Method to get all the Greetings from the Database.
	 * 
	 * @return Collection<Greeting> list of Greeting entities
	 */
	@RequestMapping(value = "/visitor/api/greetings", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<Greeting>> getGreetings() {
		Collection<Greeting> greetings = greetingService.findAll();
		return new ResponseEntity<Collection<Greeting>>(greetings, HttpStatus.OK);
	}

	/**
	 * Method to get the Greeting entity for the requested id
	 * 
	 * @param id Greeting id
	 * @return Greeting Greeting entity
	 */
	@RequestMapping(value = "/api/greetings/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Greeting> getGreeting(@PathVariable Long id) {
		Greeting greeting = greetingService.find(id);
		if (greeting == null) {
			return new ResponseEntity<Greeting>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Greeting>(greeting, HttpStatus.OK);
	}

	/**
	 * Method to save the Greeting
	 * 
	 * @param greeting entity to be saved
	 * @return Greeting Greeting entity
	 */
	@RequestMapping(value = "/api/greetings/save", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Greeting> createGreeting(@RequestBody Greeting greeting) {
		Greeting g1 = greetingService.create(greeting);
		if (g1 == null) {
			return new ResponseEntity<Greeting>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Greeting>(g1, HttpStatus.CREATED);
	}

	/**
	 * Method to update the Greeting for requested id
	 * 
	 * @param greeting
	 * @return Greeting Greeting entity
	 */
	@RequestMapping(value = "/api/greetings/update/{id}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Greeting> updateGreeting(@RequestBody Greeting greeting) {
		Greeting g1 = greetingService.update(greeting);
		if (g1 == null) {
			return new ResponseEntity<Greeting>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Greeting>(g1, HttpStatus.OK);
	}

	/**
	 * Method for deleting the Greeting of id
	 * 
	 * @param id
	 * @param greeting
	 * @return
	 */
	@RequestMapping(value = "/api/greetings/delete/{id}", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Greeting> deleteGreetings(@PathVariable("id") Long id, @RequestBody Greeting greeting) {
		greetingService.delete(id);
		return new ResponseEntity<Greeting>(HttpStatus.NO_CONTENT);
	}

	/**
	 * Method to send the email
	 * 
	 * @param id
	 * @param waitForAsynResults
	 * @return
	 */
	@RequestMapping(value = "/api/greetings/{id}/send", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Greeting> sendEmailGreeting(@PathVariable("id") Long id,
			@RequestParam(value = "wait", defaultValue = "false") boolean waitForAsynResults) {
		Greeting greeting = null;
		try {
			greeting = greetingService.find(id);
			if (greeting == null) {
				return new ResponseEntity<Greeting>(HttpStatus.NO_CONTENT);
			}
		} catch (Exception e) {

		}
		return null;
	}

}
