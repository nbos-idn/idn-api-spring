 package com.wavelabs.ws.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wavelabs.ws.model.Greeting;

/**
 * @author bikshapathi
 *
 */
@Repository
public interface GreetingRepo extends JpaRepository<Greeting, Long>{

}


