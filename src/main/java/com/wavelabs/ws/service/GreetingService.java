package com.wavelabs.ws.service;

import java.util.Collection;

import com.wavelabs.ws.model.Greeting;

/**
 * Interface for Greeting Service
 * 
 * @author bikshapathi
 *
 */
public interface GreetingService {
	/**
	 * Method to get all the Greetings entities
	 */
	public Collection<Greeting> findAll();

	/**
	 * Method to get the Greetings entity for the requested id
	 */
	public Greeting find(Long id);

	/**
	 * Method to create the Greeting
	 */
	public Greeting create(Greeting greeting);

	/**
	 * Method to update the Greeting
	 */
	public Greeting update(Greeting greeting);

	/**
	 * Method to delete the Greeting entity
	 */
	public void delete(Long id);

}
