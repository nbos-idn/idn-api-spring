package com.wavelabs.ws.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.wavelabs.ws.model.Greeting;
import com.wavelabs.ws.repository.GreetingRepo;

/**
 * Class for Greeting service
 * 
 * @author bikshapathi
 *
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class GreetingServiceImpl implements GreetingService {

	@Autowired
	private GreetingRepo greetingRepo;

	/**
	 * Method to get all the Greetings entities
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Collection<Greeting> findAll() {
		Collection<Greeting> greetings = greetingRepo.findAll();
		return greetings;
	}

	/**
	 * Method to get the Greetings entity for the requested id
	 */
	@Override
	@Cacheable(value = "greetings", key = "#id")
	public Greeting find(Long id) {
		Greeting greeting = greetingRepo.findOne(id);

		return greeting;
	}

	/**
	 * Method to create the Greeting
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	@CachePut(value = "greetings", key = "#result.id")
	public Greeting create(Greeting greeting) {
		if (greeting.getId() != null) {
			// we cannot insert an object with a specified ID value
			return null;
		}
		Greeting g1 = greetingRepo.save(greeting);
		return g1;
	}

	/**
	 * Method to update the Greeting
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	@CachePut(value = "greetings", key = "#greeting.id")
	public Greeting update(Greeting greeting) {
		Greeting greetingPersisted = greetingRepo.findOne(greeting.getId());
		if (greetingPersisted == null) {
			// Cannot update Greeting, as it is not already exists
			return null;
		}
		Greeting g1 = greetingRepo.save(greeting);
		return g1;
	}

	/**
	 * Method to delete the Greeting entity
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	@CacheEvict(value = "greetings", key = "#id")
	public void delete(Long id) {
		greetingRepo.delete(id);
	}

}
