package com.wavelabs;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.wavelabs.utils.Constants;

/**
 * This the Filter class to filter all the User requests for Auth tokens. This
 * would validate if the requested URL required user login. If it requires user
 * login, this class would authenticate the request with NBOS.in
 * 
 * @author bikshapathi
 *
 */
@Component
public class SpringFilter implements Filter {
	@Autowired
	private Environment env;

	private static Logger logger = LoggerFactory.getLogger(SpringFilter.class);

	private AuthenticationTokenCache authTokenCache = AuthenticationTokenCache.getInstance();

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig config) throws ServletException {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() {
	}

	/**
	 * Method to filter all the user requests and authenticate them
	 * 
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest,
	 *      javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		final HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		if (needLogin(httpServletRequest)) {
			String accessToken = httpServletRequest.getHeader(Constants.ACCESS_TOKEN);
			String url = env.getProperty(Constants.AUTH_URL);
			String moduleKey = env.getProperty(Constants.MODULE_KEY_LABEL);
			String authorization = Constants.BEARER + env.getProperty(Constants.VERIFY_TOKEN);

			Assert.notNull(accessToken, "Access token cannot be empty");
			Assert.notNull(url, "NBOS.in url cannot be empty");
			Assert.notNull(moduleKey, "Module Key cannot be empty");
			Assert.notNull(env.getProperty(Constants.VERIFY_TOKEN), "Verify Token cannot be empty");
			logger.info("validating the user Authentication Token");
			if (authTokenCache.validateAuthToken(accessToken, authorization, moduleKey, url)) {
				processRequest(request, response, chain);
			}
		} else {
			processRequest(request, response, chain);
		}

	}

	/**
	 * check if the user request needs login, based on the URL
	 * 
	 * @param request
	 * @return
	 */
	private Boolean needLogin(final HttpServletRequest httpServletRequest) {
		Boolean needed = Boolean.TRUE;
		String requestURI = httpServletRequest.getRequestURI();
		if (requestURI.indexOf("/visitor/") > 0) {
			needed = Boolean.FALSE;
		}
		return needed;
	}

	/**
	 * Method to proceed to the next element in the chain
	 * 
	 * @param request
	 * @param response
	 */
	private void processRequest(ServletRequest request, ServletResponse response, FilterChain chain)
			throws ServletException, IOException {
		// call the underlying rest servlet
		chain.doFilter(request, response);
	}

}
